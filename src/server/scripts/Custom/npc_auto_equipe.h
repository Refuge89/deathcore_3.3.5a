/*
 * Copyright (C) 2013-2015 DeathCore <http://www.noffearrdeathproject.net/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "ScriptPCH.h"
#include "Config.h"
#include "Scritp_auto_equipe.h"

bool Relogar = true;
bool AUTO_EQUIPADOR = true;
bool AUTO_TALENTOS = true;
int TIPO_DE_TRENADOR_ORIGINAL;

int Arena_Season; //8 = Arena Season 8 //7 = Arena Season 7  //6 = Arena Season 6  //5 = Arena Season 5  //e etc..
std::string Arena_Season_Name;

enum ItemsMENU
{
	MENU_SAIR = 99901,
	MENU_VOLTAR = 99902,
	REMOVER_EQUIP = 99903,
	DUAL_TELENTS = 99904,
	RESTAR_LEVEL80 = 99905,
	GLYPH_NPC_MENU = 99906,
	RESET_TALENTOS = 99907,
	AUTO_TALENTOS_MENU = 99908,
	TRENADOR_CLASS = 80,
	EQUIPAR_ITEM_AUTOMATICOS = 90,
	MENUTRENADOR = 99912,
	TRENADORCLASS = 99916,
	MENU_TRENADOR_ORIGINAL = 99913,
	MENU_TRENADOR_SPELLSUMMON = 99914,
	MENU_EQUIPAR_ITEM_AUTOMATICOS = 99915,
	MENU_WARRIOR,
	MENU_PALADIN,
	MENU_HUNTER,
	MENU_ROGUE,
	MENU_PRIEST,
	MENU_DEATH_K,
	MENU_SHAMAN,
	MENU_MAGE,
	MENU_WARLOCK,
	MENU_DRUID,

	SPELL_TELEPORT_VISUAL = 64446
};
struct TabelaItens
{
	uint32 itemID;
	uint32  pClass;
	uint8 ItemSet, PTalentoTipo;
	
};
std::vector<TabelaItens> TabelaItensList;
bool CarregarAutoEquipeItens()
{
	TabelaItensList.clear();

	TC_LOG_INFO("server.loading", "Carregando Itens para AutoEquipe...");
	uint32 sMSTime = getMSTime();

	QueryResult resultados = CharacterDatabase.PQuery("SELECT ItemID, pClass, ItemSet, pTalentoTipo FROM `item_autoEquipe`");
	if (!resultados)
		return true;
	uint16 contador = 0;
	do
	{
		Field* fields = resultados->Fetch();
		TabelaItens Itens;
		Itens.itemID = fields[0].GetUInt32();
		Itens.pClass = fields[1].GetInt32();
		Itens.ItemSet = fields[2].GetInt8();
		Itens.PTalentoTipo = fields[3].GetInt8();

		ItemTemplate const* pProto = sObjectMgr->GetItemTemplate(Itens.itemID);
		if (!pProto)
		{
			TC_LOG_ERROR("server.loading", "itemID [%u] nao-existe", Itens.itemID);
			continue;
		}		
		TabelaItensList.push_back(Itens);
		++contador;
	} while (resultados->NextRow());

	TC_LOG_INFO("server.loading", ">> Carregados %u Itens para AutoEquipe em %u ms", contador, GetMSTimeDiffToNow(sMSTime));

	return true;
}
//===========Equipar player
bool EquiparItems(Player* player)
{
	PlayerInfo const* info = sObjectMgr->GetPlayerInfo(player->getRace(), player->getClass());
	for (uint8 i = 0; i < PLAYER_SLOTS_COUNT; i++)
	{
		for (PlayerCreateInfoActions::const_iterator action_itr = info->action.begin(); action_itr != info->action.end(); ++action_itr)
			player->addActionButton(action_itr->button, action_itr->action, action_itr->type);
		// original items
		if (CharStartOutfitEntry const* oEntry = GetCharStartOutfitEntry(player->getRace(), player->getClass(), player->getGender()))
		{
			for (int j = 0; j < MAX_OUTFIT_ITEMS; ++j)
			{
				if (oEntry->ItemId[j] <= 0)
					continue;
				uint32 itemId = oEntry->ItemId[j];
				// just skip, reported in ObjectMgr::LoadItemTemplates
				ItemTemplate const* iProto = sObjectMgr->GetItemTemplate(itemId);
				if (!iProto)
					continue;
				// BuyCount by default
				uint32 count = iProto->BuyCount;
				// special amount for food/drink
				if (iProto->Class == ITEM_CLASS_CONSUMABLE && iProto->SubClass == ITEM_SUBCLASS_FOOD)
				{
					switch (iProto->Spells[0].SpellCategory)
					{
					case SPELL_CATEGORY_FOOD: // food
						count = player->getClass() == CLASS_DEATH_KNIGHT ? 10 : 4;
						break;
					case SPELL_CATEGORY_DRINK: // drink
						count = 2;
						break;
					}
					if (iProto->GetMaxStackSize() < count)
						count = iProto->GetMaxStackSize();
				}

				uint32 pClass = player->getClassMask();
				uint8 pLevel = player->getLevel();

				for (uint32 i = 0; i < TabelaItensList.size(); ++i)
				{
					TabelaItens &Itens = TabelaItensList[i];
					ItemTemplate const* pProto = sObjectMgr->GetItemTemplate(Itens.itemID);

					if (!(Itens.pClass & pClass)) continue;

					if (pProto->RequiredLevel > pLevel) continue;

				//	if (!(Itens.itemID & pProto->AllowableClass)) continue;
				
					if (!(Itens.ItemSet & Arena_Season)) continue;

					if (player->GetItemByEntry(Itens.itemID)){
						player->DestroyItemCount(Itens.itemID, 99, true); // remover
					}
					if (player->HasItemCount(Itens.itemID, 1)) continue;

					if (Itens.itemID == 51809){ // bag
						player->StoreNewItemInBestSlots(51809, 3);
					}
					if (Itens.itemID == 52021){
						player->StoreNewItemInBestSlots(52021, 200);
						player->SetAmmo(52021);
					}

					player->StoreNewItemInBestSlots(Itens.itemID, 1);										
				}
				return true;

			}
		}
	}
	return true;
}//========Fim equipar player


//===========Remover items Slot que n�o tam na lista Aray{}
bool RemoverItemsSlostNoAray(Player* player)
{ // remove items do corpo e n�o remove da bag // verifica sim se esta com item no corpo
	if (player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_HEAD))
	player->MoveItemFromInventory(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_HEAD, true);

	if (player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_NECK))
	player->MoveItemFromInventory(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_NECK, true);

	if (player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_SHOULDERS))
	player->MoveItemFromInventory(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_SHOULDERS, true);

	if (player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_BODY))
	player->MoveItemFromInventory(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_BODY, true);

	if (player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_CHEST))
	player->MoveItemFromInventory(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_CHEST, true);

	if (player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_WAIST))
	player->MoveItemFromInventory(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_WAIST, true);

	if (player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_LEGS))
	player->MoveItemFromInventory(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_LEGS, true);

	if (player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_FEET))
	player->MoveItemFromInventory(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_FEET, true);

	if (player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_WRISTS))
	player->MoveItemFromInventory(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_WRISTS, true);

	if (player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_HANDS))
	player->MoveItemFromInventory(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_HANDS, true);

	if (player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_FINGER1))
	player->MoveItemFromInventory(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_FINGER1, true);

	if (player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_FINGER2))
	player->MoveItemFromInventory(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_FINGER2, true);

	if (player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_TRINKET1))
	player->MoveItemFromInventory(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_TRINKET1, true);

	if (player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_TRINKET2))
	player->MoveItemFromInventory(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_TRINKET2, true);

	if (player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_BACK))
	player->MoveItemFromInventory(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_BACK, true);

	if (player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND))
	player->MoveItemFromInventory(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND, true);

	if (player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND))
	player->MoveItemFromInventory(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND, true);

	if (player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_RANGED))
	player->MoveItemFromInventory(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_RANGED, true);

	if (player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_TABARD))
	player->MoveItemFromInventory(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_TABARD, true);
	
	return true;
} //===========Fim Remover items Slot que n�o tam na lista Aray{}

void AplicarTalentosSpell(Player* player)
{
	player->ResetTalents(true);

/*	QueryResult result = CharacterDatabase.PQuery(ColunaTalentRank);
	if (!result)
	{
		ChatHandler(player->GetSession()).PSendSysMessage("Sem resultado");
	}
	else
	{
		Field * fields = NULL;
		do
		{
			Field * fields = result->Fetch();

			uint32 spell = fields[0].GetUInt32();
			player->AddTalent(spell, 0, true);

			player->SetFreeTalentPoints(0);

			 //			CharacterDatabase.PExecute("REPLACE INTO `character_spell` (`guid`, `spell`,`disabled`) VALUES(%u,%u,%u)", player->GetGUIDLow(), LearnSpell[i][0], LearnSpell[i][2]);

		} while (result->NextRow());
		return;
	} */
	//return;
}

void AplicarTalentos(Player* player)
{ 
	player->ResetTalents(true);

/*	QueryResult result = CharacterDatabase.PQuery(ColunaTalentRank);
	if (!result)
	{
		TC_LOG_ERROR("network", "Nao tem talentos na database [%s] para a Class %u Tipo items: ", ColunaTalentRank, player->getClass());
	}
	else
	{
		Field * fields = NULL;

		do
		{		
			Field * fields = result->Fetch();

			uint32 spell = fields[0].GetUInt32();							
			player->LearnSpell(spell, false);

			if (!spell){
				TC_LOG_ERROR("network", "ERRO: Ao aplicar talentos Player [%s]", player->GetName().c_str());
				continue;
			}
			player->AddTalent(spell, 0, true);
						
			player->SetFreeTalentPoints(0);

		} while (result->NextRow());			
		return;
	} */
	//return;
}
// ===============================================================
void TempoParaRelogar()
{
	clock_t temp;
	temp = clock() + CLOCKS_PER_SEC + 9000;
	while (clock() < temp) {}
}
void RelogarPlayer(Player* plrs)
{
	while (1)
	{
		ChatHandler(plrs->GetSession()).PSendSysMessage("Voce vai ser relogado em alguns segundos!..");
		TempoParaRelogar();
		plrs->GetSession()->LogoutRequest(100000.0f);
		break;
	}
}
// ==================================================================
void ExecutarALLItens(Player* player, uint32 plevelup)
{
	if (AUTO_EQUIPADOR == true){
		TreinarSpell_autoEquipe(player, plevelup);
		RemoverItemsSlostNoAray(player);
		EquiparItems(player); //Equiplar player	
	}
	if (Relogar == true){
		//	RelogarPlayer(player);
		//	player->GetSession()->LogoutRequest(100000.0f);
	}
	player->UpdateMaxHealth(); // Update max Health (for add bonus from stamina)
	player->SetFullHealth();
	if (player->getPowerType() == POWER_MANA)
	{
		player->UpdateMaxPower(POWER_MANA); // Update max Mana (for add bonus from intellect)
		player->SetPower(POWER_MANA, player->GetMaxPower(POWER_MANA));
	}
	if (player->getPowerType() == POWER_RUNIC_POWER)
	{
		player->SetPower(POWER_RUNE, 8);
		player->SetMaxPower(POWER_RUNE, 8);
		player->SetPower(POWER_RUNIC_POWER, 0);
		player->SetMaxPower(POWER_RUNIC_POWER, 1000);
	}
	player->UpdateAllStats();
	player->SaveToDB();
}
void ExecutarALLTalentos(Player* player, uint32 plevelup)
{
	TreinarSpell_autoEquipe(player, plevelup);

	if (AUTO_EQUIPADOR == true){			
		RemoverItemsSlostNoAray(player);
		EquiparItems(player); //Equiplar player	
	}
	if (AUTO_TALENTOS == true){
		LearnTalentos(player);		
	}
	if (Relogar == true){
		//	RelogarPlayer(player);
		//	player->GetSession()->LogoutRequest(100000.0f);
	}	
	player->UpdateMaxHealth(); // Update max Health (for add bonus from stamina)
	player->SetFullHealth();
	if (player->getPowerType() == POWER_MANA)
	{
		player->UpdateMaxPower(POWER_MANA); // Update max Mana (for add bonus from intellect)
		player->SetPower(POWER_MANA, player->GetMaxPower(POWER_MANA));
	}
	if (player->getPowerType() == POWER_RUNIC_POWER)
	{
		player->SetPower(POWER_RUNE, 8);
		player->SetMaxPower(POWER_RUNE, 8);
		player->SetPower(POWER_RUNIC_POWER, 0);
		player->SetMaxPower(POWER_RUNIC_POWER, 1000);
	}
	player->UpdateAllStats();
	player->SaveToDB();
}

void TreinadorAutoEquipeAtalho(Player* player, uint32 trainer)
{
	TreinarSpell_autoEquipe(player, trainer);
	
	player->UpdateMaxHealth(); // Update max Health (for add bonus from stamina)
	player->SetFullHealth();
	if (player->getPowerType() == POWER_MANA)
	{
		player->UpdateMaxPower(POWER_MANA); // Update max Mana (for add bonus from intellect)
		player->SetPower(POWER_MANA, player->GetMaxPower(POWER_MANA));
	}
	if (player->getPowerType() == POWER_RUNIC_POWER)
	{
		player->SetPower(POWER_RUNE, 8);
		player->SetMaxPower(POWER_RUNE, 8);
		player->SetPower(POWER_RUNIC_POWER, 0);
		player->SetMaxPower(POWER_RUNIC_POWER, 1000);
	}
	player->UpdateAllStats();
	player->SaveToDB();
	return;
}

// === DualTelatos====
void EspecialzacaoDuelTalents(Player* player)
{
	if (player->GetSpecsCount() == 1)
	{
		if (player->getLevel() >= sWorld->getIntConfig(CONFIG_MIN_DUALSPEC_LEVEL))
		{
			if (player->GetMoney() >= 3 * GOLD){ // 30.000 = 3k
				player->CastSpell(player, 63680, false, NULL, NULL, player->GetGUID()); //63651 para tirar
				player->CastSpell(player, 63624, true, NULL, NULL, player->GetGUID());
				player->CLOSE_GOSSIP_MENU();
				ChatHandler(player->GetSession()).PSendSysMessage("|cffFFFFFFEspecializacao Talentos Duplos adiquirida com sucesso!|r");
				player->SaveToDB();
			}
			else
			{
				ChatHandler(player->GetSession()).PSendSysMessage("|cffFF0000Voce presisa de|r %u |cffFF0000de GOLD  para comprar Especializacao Talentos Duplos|r", 3 * GOLD);
				player->CLOSE_GOSSIP_MENU();
			}
		}
		else
		{
			ChatHandler(player->GetSession()).PSendSysMessage("|cffFF0000Voce so pode pegar Especializacao Talentos Duplos apartido level|r %u", sWorld->getIntConfig(CONFIG_MIN_DUALSPEC_LEVEL));
			player->CLOSE_GOSSIP_MENU();
		}
	}
	else
	{
		ChatHandler(player->GetSession()).PSendSysMessage("|cffFF0000Voce ja tem Especializacao Talentos Duplos|r");
		player->CLOSE_GOSSIP_MENU();
	}
}// === DualTelatos====Fim

// === Reset Talentos ===
void RestarTalentos(Player* player, Creature* creature)
{
	if (player->GetFreeTalentPoints() == +player->CalculateTalentsPoints()){
		ChatHandler(player->GetSession()).PSendSysMessage("|cffFF0000Seus talentos ja estam resetados!|r");
		player->CLOSE_GOSSIP_MENU();
	}
	else
	{
		player->CLOSE_GOSSIP_MENU();
		creature->CastSpell(player, 17251, false);
		player->ResetTalents();
		player->SetFreeTalentPoints(player->CalculateTalentsPoints());
		ChatHandler(player->GetSession()).PSendSysMessage("|cff00FF00Talentos resetados com sucesso!|r");
		player->SendTalentsInfoData(false);
		player->SaveToDB();
	}
}// === Reset Talentos ===Fim

// Tempo para summonar a pr�xima vez
std::map<uint32, uint32> MemoriaTempo;
uint32 Minutos = 3;
uint32 CalculateMinutos(uint32 m_time)
{
	if (m_time <= 60000)
		Minutos = 1;
	else if (m_time <= 120000)
		Minutos = 2;
	else if (m_time <= 180000) // 180000 = 3 minutes
		Minutos = 3;

	return Minutos;
}

// N�o requer MultiTrainer nem MultiVendor
void SumonarTrainerPorSpell(Player* player, Creature* _creature, uint32 TreinadorClasses)
{
	bool SumonarEM = true;
	if (!MemoriaTempo.empty())
	{
		std::map<uint32, uint32>::iterator itr = MemoriaTempo.find(player->GetGUIDLow());
		if (itr != MemoriaTempo.end())
		if (GetMSTimeDiffToNow(itr->second) < 60000) // 3 minutos = 180000 | 2 minutos = 120000  | 1 minutos = 60000  e etc.
		{
			ChatHandler(player->GetSession()).PSendSysMessage("|cffFF0000Espere %u minuto(s) para poder summonar novamente.|r", CalculateMinutos(GetMSTimeDiffToNow(itr->second)));
			SumonarEM = false;
		}
		else
		{
			MemoriaTempo.erase(player->GetGUIDLow());
			SumonarEM = true;
		}
	}
	// Tempo em que o npc fica sumonado e te seguinto
	uint32 TEMPO__SUMMON = 60000; // 3 minutos = 180000 | 2 minutos = 120000  | 1 minutos = 60000  e etc.

	float x, y, z;
	player->GetPosition(x, y, z);

#define summDruid player->SummonCreature(26324, x, y-1, z, +1, TEMPSUMMON_TIMED_DESPAWN, TEMPO__SUMMON)
#define summHunter player->SummonCreature(26325, x, y-1, z, +1, TEMPSUMMON_TIMED_DESPAWN, TEMPO__SUMMON)
#define summMage player->SummonCreature(26326, x, y-1, z, +1, TEMPSUMMON_TIMED_DESPAWN, TEMPO__SUMMON)
#define summPaladin player->SummonCreature(26327, x, y-1, z, +1, TEMPSUMMON_TIMED_DESPAWN, TEMPO__SUMMON)
#define summPriest player->SummonCreature(26328, x, y-1, z, +1, TEMPSUMMON_TIMED_DESPAWN, TEMPO__SUMMON)
#define summRogue player->SummonCreature(26329, x, y-1, z, +1, TEMPSUMMON_TIMED_DESPAWN, TEMPO__SUMMON)
#define summShaman player->SummonCreature(26330, x, y-1, z, +1, TEMPSUMMON_TIMED_DESPAWN, TEMPO__SUMMON)
#define summWarlock player->SummonCreature(26331, x, y-1, z, +1, TEMPSUMMON_TIMED_DESPAWN, TEMPO__SUMMON)
#define summWarrioR player->SummonCreature(26332, x, y-1, z, +1, TEMPSUMMON_TIMED_DESPAWN, TEMPO__SUMMON)
#define summDK player->SummonCreature(28474, x, y-1, z, +1, TEMPSUMMON_TIMED_DESPAWN, TEMPO__SUMMON)
#define MeSeguir GetMotionMaster()->MoveFollow(player, PET_FOLLOW_DIST, _creature->GetFollowAngle() + 8)

	player->PlayerTalkClass->ClearMenus();
	if (SumonarEM){
		MemoriaTempo[player->GetGUIDLow()] = getMSTime();

		switch (TreinadorClasses = player->getClass())
		{
		case CLASS_WARRIOR:
			summWarrioR->MeSeguir;
			_creature->CastSpell(player, 44328, false);
			break;
		case CLASS_PALADIN:
			summPaladin->MeSeguir;
			_creature->CastSpell(player, 44328, false);
			break;
		case CLASS_DEATH_KNIGHT:
			summDK->MeSeguir;
			_creature->CastSpell(player, 44328, false);
			break;
		case CLASS_DRUID:
			summDruid->MeSeguir;
			_creature->CastSpell(player, 44328, false);
			break;
		case CLASS_HUNTER:
			summHunter->MeSeguir;
			_creature->CastSpell(player, 44328, false);
			break;
		case CLASS_MAGE:
			summMage->MeSeguir;
			_creature->CastSpell(player, 44328, false);
			break;
		case CLASS_PRIEST:
			summPriest->MeSeguir;
			_creature->CastSpell(player, 44328, false);
			break;
		case CLASS_ROGUE:
			summRogue->MeSeguir;
			_creature->CastSpell(player, 44328, false);
			break;
		case CLASS_WARLOCK:
			summWarlock->MeSeguir;
			_creature->CastSpell(player, 44328, false);
			break;
		case CLASS_SHAMAN:
			summShaman->MeSeguir;
			_creature->CastSpell(player, 44328, false);
			break;

		default:
			return;
		}
	}
	return;
	/*
	player->CastSpell(player, 47359, false); // Trenador Druid
	player->CastSpell(player, 47360, false); // Trenador Hunter
	player->CastSpell(player, 47361, false); // Tranador Mage
	player->CastSpell(player, 47362, false); // Tranador Paladin
	player->CastSpell(player, 47363, false); // Tranador Priest
	player->CastSpell(player, 47364, false); // Tranador Rogue
	player->CastSpell(player, 47365, false); // Tranador Shaman
	player->CastSpell(player, 47366, false); // Tranador Warlock
	player->CastSpell(player, 47367, false); // Trenador WarrioR
	*/
}

/*
// Requer MultiTrainer
void TrenadoresOriginais(Player* player, Creature* creature, uint8 TrainerClass)
{
	uint8 GetRace = player->GetTeam() == ALLIANCE;

	switch (TrainerClass = player->getClass())
	{
	case CLASS_WARRIOR:
		if (GetRace)
			player->GetSession()->SendTrainerList(creature->GetGUID(), 914);
		player->GetSession()->SendTrainerList(creature->GetGUID(), 3408);
			break;
	case CLASS_PALADIN:
		if (GetRace)
			player->GetSession()->SendTrainerList(creature->GetGUID(), 5491);
		player->GetSession()->SendTrainerList(creature->GetGUID(), 23128);
		break;
	case CLASS_DEATH_KNIGHT:
			player->GetSession()->SendTrainerList(creature->GetGUID(), 28474);
			break;
	case CLASS_DRUID:
		if (GetRace)
			player->GetSession()->SendTrainerList(creature->GetGUID(), 5504);
		player->GetSession()->SendTrainerList(creature->GetGUID(), 16655);
		break;
	case CLASS_HUNTER:
		if (GetRace)
			player->GetSession()->SendTrainerList(creature->GetGUID(), 5515);
		player->GetSession()->SendTrainerList(creature->GetGUID(), 3352);
		break;
	case CLASS_MAGE:
		if (GetRace)
			player->GetSession()->SendTrainerList(creature->GetGUID(), 5498);
		player->GetSession()->SendTrainerList(creature->GetGUID(), 5885);
		break;
	case CLASS_PRIEST:
		if (GetRace)
			player->GetSession()->SendTrainerList(creature->GetGUID(), 5489);
		player->GetSession()->SendTrainerList(creature->GetGUID(), 6018);
		break;
	case CLASS_ROGUE:
		if (GetRace)
			player->GetSession()->SendTrainerList(creature->GetGUID(), 13283);
		player->GetSession()->SendTrainerList(creature->GetGUID(), 3328);
		break;
	case CLASS_SHAMAN:
		if (GetRace)
			player->GetSession()->SendTrainerList(creature->GetGUID(), 20407);
		player->GetSession()->SendTrainerList(creature->GetGUID(), 3344);
		break;
	case CLASS_WARLOCK:
		if (GetRace)
			player->GetSession()->SendTrainerList(creature->GetGUID(), 5496);
		player->GetSession()->SendTrainerList(creature->GetGUID(), 3325);
		break;

	}
	return;
}

// Requer MultiVendor 
bool VendedorClyphs(Player* player, Creature* creature)
{
	if (player->getClass() == CLASS_WARRIOR)
		player->GetSession()->SendListInventory(creature->GetGUID(), 99901);
	if (player->getClass() == CLASS_PALADIN)
		player->GetSession()->SendListInventory(creature->GetGUID(), 99902);
	if (player->getClass() == CLASS_MAGE)
		player->GetSession()->SendListInventory(creature->GetGUID(), 99903);
	if (player->getClass() == CLASS_HUNTER)
		player->GetSession()->SendListInventory(creature->GetGUID(), 99904);
	if (player->getClass() == CLASS_DRUID)
		player->GetSession()->SendListInventory(creature->GetGUID(), 99905);
	if (player->getClass() == CLASS_WARLOCK)
		player->GetSession()->SendListInventory(creature->GetGUID(), 99906);
	if (player->getClass() == CLASS_PRIEST)
		player->GetSession()->SendListInventory(creature->GetGUID(), 99907);
	if (player->getClass() == CLASS_ROGUE)
		player->GetSession()->SendListInventory(creature->GetGUID(), 99908);
	if (player->getClass() == CLASS_DEATH_KNIGHT)
		player->GetSession()->SendListInventory(creature->GetGUID(), 99909);
	if (player->getClass() == CLASS_SHAMAN)
		player->GetSession()->SendListInventory(creature->GetGUID(), 99910);

	return true;
} */

/*
class PlayerAutoEquipar : public PlayerScript
{
public:
	PlayerAutoEquipar() : PlayerScript("PlayerAutoEquipar") {}

	void OnLevelChanged(Player* player, uint8 )
	{
            
	}
	void OnLogin(Player* player, bool firstLogin)
	{
		
			
	}
	bool OnGossipeHelo(Player* player, uint32 )
	{

	}
};
*/

/*  player->getClassMask();
    1	  1  Warrior                  
	2	  2	 Paladin
	4	  3	 Hunter
	8	  4	 Rogue
	16	  5	 Priest
	32	  6	 Death Knight
	64	  7	 Shaman
	128	  8	 Mage
	256	  9	 Warlock
	1024 11	 Druid	
	
	player->GetRaceMask()
	1 | 1		Human
	2 | 2		Orc
	4 | 3		Dwarf
	8 | 4		Night Elf
	16 | 5		Undead
	32 | 6		Tauren
	64 | 7		Gnome
	128 | 8		Troll
	256 | 9		Goblin
	512 | 10	Blood Elf
	1024 | 11	Draenei

	player->GetTeamId()
	ALLIANCE = 0
	HORDE    = 1 

	player->GetTeam()
	ALLIANCE = 469
	HORDE    = 67   	*/

class AutoEquipe_commands : public CommandScript
{
public:
	AutoEquipe_commands() : CommandScript("AutoEquipe_commands") { }
	ChatCommand* GetCommands() const
	{
		static ChatCommand AutoEquipeCommandTable[] =
		{
			{ "autoequipe", SEC_PLAYER, false, &HandleReloadAutoEquipeCommand, "", NULL },
			{ NULL, 0, false, NULL, "", NULL }
		};
		return AutoEquipeCommandTable;
	}
	static bool HandleReloadAutoEquipeCommand(ChatHandler * handler, const char * args)
	{
		Player* player = handler->GetSession()->GetPlayer();
		if (CarregarAutoEquipeSpellTalentos()){
			TC_LOG_INFO("server.loading", "Carregando spellTalentos para AutoEquipe...");
			handler->SendGlobalGMSysMessage("DB tabela character_talent_autoequipe carregada.");
		}
		return true;
	}
};

class npc_auto_equipe_WorldScript : public WorldScript
{
public:
	npc_auto_equipe_WorldScript() : WorldScript("npc_auto_equipe_WorldScript") { }


	void OnConfigLoad(bool)
	{
		Arena_Season = sConfigMgr->GetIntDefault("Arena.Season", 0);
		Arena_Season_Name = sConfigMgr->GetStringDefault("Arena.Season_Name", "Arena Season x");
		TIPO_DE_TRENADOR_ORIGINAL = sConfigMgr->GetIntDefault("Tipo.Treinador.Original", 0);

		LoadDataFromDataBase();
	}
	void LoadDataFromDataBase()
	{
	//	CarregarAutoEquipe();
		CarregarAutoEquipeItens();
		CarregarAutoEquipeSpellTalentos();
	}		
	
	void OnStartup() override
	{
		
	}
};

