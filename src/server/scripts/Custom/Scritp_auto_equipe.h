/*
 * Copyright (C) 2013-2015 DeathCore <http://www.noffearrdeathproject.net/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "ScriptPCH.h"
#include "Language.h"

uint32 SIg[] = {
	64380, 23885, 23880, 44461, 25346, 10274, 10273, 8418, 8419, 7270, 7269, 7268, 54648, 12536, 24530, 70909,
	12494, 57933, 24224, 27095, 27096, 27097, 27099, 32841, 56131, 56160, 56161, 48153, 34754, 64844, 64904, 48085,
	33110, 48084, 28276, 27874, 27873, 7001, 49821, 53022, 47757, 47750, 47758, 47666, 53001, 52983, 52998, 52986,
	52987, 52999, 52984, 53002, 53003, 53000, 52988, 52985, 42208, 42209, 42210, 42211, 42212, 42213, 42198, 42937,
	42938, 12484, 12485, 12486, 44461, 55361, 55362, 34913, 43043, 43044, 38703, 38700, 27076, 42844, 42845, 64891,
	25912, 25914, 25911, 25913, 25902, 25903, 27175, 27176, 33073, 33074, 48822, 48820, 48823, 48821, 20154, 25997,
	20467, 20425, 67, 26017, 34471, 53254, 13812, 14314, 14315, 27026, 49064, 49065, 60202, 60210, 13797, 14298,
	14299, 14300, 14301, 27024, 49053, 49054, 52399, 1742, 24453, 53548, 53562, 52016, 26064, 35346, 57386, 57389,
	57390, 57391, 57392, 57393, 55509, 35886, 43339, 45297, 45298, 45299, 45300, 45301, 45302, 49268, 49269, 8349,
	8502, 8503, 11306, 11307, 25535, 25537, 61650, 61654, 63685, 45284, 45286, 45287, 45288, 45289, 45290, 45291,
	45292, 45293, 45294, 45295, 45296, 49239, 49240, 26364, 26365, 26366, 26367, 26369, 26370, 26363, 26371, 26372,
	49278, 49279, 32176, 32175, 21169, 47206, 27285, 47833, 47836, 42223, 42224, 42225, 42226, 42218, 47817, 47818,
	42231, 42232, 42233, 42230, 48466, 44203, 44205, 44206, 44207, 44208, 48444, 48445, 33891, 52374, 57532, 59921,
	52372, 49142, 52375, 47633, 47632, 52373, 50536, 27214, 47822, 11682, 11681, 5857, 1010, 24907, 24905, 53227,
	61391, 61390, 61388, 61387, 64801, 5421, 9635, 1178, 20186, 20185, 20184, 20187, 25899, 24406, 50581, 30708,
	8946, 42459, 23455, 48076
};
uint32 SRestante[][99]
{		// Warrior 
	{ 3127, 264, 266, 5011, 15590, 200, 227, 2567, 674, 750, 12678 },
		//DK 
	{ 198, 199, 3714, 53323, 53331, 53341, 53342, 53343, 53344, 54446, 54447, 62158, 70164 },
	//Druid
	{ 15590, 200, 5225, 20719, 62600 },
	// Hunter 
	{ 674, 42459, 8737, 15590, 2567, 227, 266, 5011, 200, 201, 1462, 3127, 6197, 19885 },
	// mage
	{ 1180, 201, /*Alliance->*/ 3562, 3561, 3565, 11416, 11419, 32266, 32271, 33690, 33691, 49359, 49360, /*Horde->*/ 3563, 3566, 3567, 11417, 11418, 11420, 32272, 35715, 35717, 32267, 49358, 49361 },
	//==Fim spell Mage======================================================================
	// paladin
	{ 750, 200, 196, 197, 199, 3127, 48950 },
	//Rogue
	{ 264, 15590, 5011, 201, 196, 198, 3127, 2836 },
	//Shaman
	{ 8737, 1180, 15590, 196, 197, 199, 6196, 66842, 66843, 66844 },
	//Warlock
	{ 201, 5500, 5784, 29858, 33388, 47836 },
};

std::vector<uint32> SpellsPIguinorar = std::vector<uint32>(SIg, SIg + sizeof(SIg) / sizeof(SIg[0]));

bool GetSpellIguinorar(uint32 IDspell)
{
	for (std::vector<uint32>::const_iterator itr = SpellsPIguinorar.begin(); itr != SpellsPIguinorar.end(); ++itr)
	if (IDspell == (*itr))
		return true;
	return false;
}
bool SpellsRestante(Player* player, uint32 SpellClasses)
{
	uint8 pLevel = player->getLevel() + 1;
	switch (SpellClasses = player->getClass())
	{
	case CLASS_WARRIOR:
		for (int i = 0; i < 11; i++)
		{
			player->LearnSpell(SRestante[0][i], false);
		}
		if (player->HasSpell(12294))
			player->LearnSpell(47486, false);

		if (player->HasSpell(20243))
			player->LearnSpell(47498, false);

		break;
	case CLASS_DEATH_KNIGHT:
		for (int i = 0; i < 13; i++)
		{
			player->LearnSpell(SRestante[1][i], false);
		}
		if (player->HasSpell(55050)) //Heart Strike Rank 1
			player->LearnSpell(55262, false);

		if (player->HasSpell(49143)) //Frost Strike Rank 1
			player->LearnSpell(55268, false);

		if (player->HasSpell(49184)) //Howling Blast Rank 1
			player->LearnSpell(51411, false);

		if (player->HasSpell(55090)) //Scourge Strike Rank 1
			player->LearnSpell(55271, false);

		if (player->HasSpell(49158)) //Corpse Explosion Rank 1
			player->LearnSpell(51328, false);

		if (player->GetFreeTalentPoints() == +25){
			player->CastSpell(player, 26410, false);
			player->ResetTalents();
			player->SetFreeTalentPoints(71);
			player->SendTalentsInfoData(false);
		}
		break;
	case CLASS_DRUID:
		for (int i = 0; i < 5; i++)
		{
			player->LearnSpell(SRestante[2][i], false);
		}
		if (player->HasSpell(50516)) //Typhoon Rank 1
			player->LearnSpell(61384, false);

		if (player->HasSpell(48505)) //Starfall Rank 1
			player->LearnSpell(53201, false);

		if (player->HasSpell(5570)) //Insect Swarm Rank 1
			player->LearnSpell(48468, false);

		if (player->HasSpell(48438)) //Wild Growth Rank 1
			player->LearnSpell(53251, false);

		if (player->HasSpell(33876) && player->HasSpell(33878) && player->HasSpell(33917))
		{
			player->LearnSpell(48564, false);
			player->LearnSpell(48566, false);
		}
		if (player->HasSpell(5487))
			player->LearnSpell(9634, false);
		break;
	case CLASS_HUNTER:
		for (int i = 0; i < 14; i++)
		{
			player->LearnSpell(SRestante[3][i], false);
		}
		if (player->HasSpell(19386)) //Wyvern String Rank 1
			player->LearnSpell(49012, false);

		if (player->HasSpell(53301)) //Explosive Shot Rank 1
			player->LearnSpell(60053, false);

		if (player->HasSpell(19306)) //Conter Attack Rank 1
			player->LearnSpell(48999, false);

		if (player->HasSpell(19434)) //Aimed Shot Rank 1
			player->LearnSpell(49050, false);
		break;
	case CLASS_MAGE:
		for (int i = 0; i < 25; i++)
		{
			SpellInfo const* spellInfo = sSpellMgr->GetSpellInfo(SRestante[4][i]);
			if (!spellInfo)	continue;
			if (!(pLevel > spellInfo->SpellLevel)) continue;

			if (!player->GetSession()->GetPlayer()->IsSpellFitByClassAndRace(SRestante[4][i])) continue;

			player->LearnSpell(SRestante[4][i], false);
		}
		if (player->HasSpell(44425)) //Arcane Barrage Rank 1
			player->LearnSpell(44781, false);

		if (player->HasSpell(11113)) //Blast Wave Rank 1
			player->LearnSpell(42945, false);

		if (player->HasSpell(31661)) //Dragon Breath  Rank 1
			player->LearnSpell(42950, false);

		if (player->HasSpell(44457)) //Living Bomb Rank 1
			player->LearnSpell(55360, false);

		if (player->HasSpell(11366)) //Pyroblast Rank 1
			player->LearnSpell(42891, false);

		if (player->HasSpell(11426)) //Ice Barrier Rank 1
			player->LearnSpell(43039, false);

		break;
	case CLASS_PALADIN:
		for (int i = 0; i < 7; i++)
		{
			SpellInfo const* spellInfo = sSpellMgr->GetSpellInfo(SRestante[5][i]);
			if (!spellInfo)	continue;
			if (!(pLevel > spellInfo->SpellLevel)) continue;
			if (player->HasSpell(SRestante[5][i])) continue;

			player->LearnSpell(SRestante[5][i], false);
		}
		if (player->GetTeam() == ALLIANCE) // spell s� para alliance
		{
			player->LearnSpell(31801, false);
			player->LearnSpell(13819, false);
			player->LearnSpell(23214, false);
		}
		else{ // spell s� para Horde
			player->LearnSpell(53736, false);
			player->LearnSpell(34769, false);
			player->LearnSpell(34767, false);
		}
		if (player->HasSpell(20925)) //Holy Shield Rank 1
			player->LearnSpell(48952, false);

		if (player->HasSpell(20473)) //Holy Shock Rank 1
			player->LearnSpell(48825, false);

		if (player->HasSpell(31935)) //Avengers Shield Rank 1
			player->LearnSpell(48827, false);

		if (player->HasSpell(20911)) //Blessing of Sanctuary Rank 1
			player->LearnSpell(25899, false);

		/* Devido ao bug de paladin nesta nova core que tem duas [Seal of Righteousness]
		N�o achei outra  forma de resolver este bug dessa nova core 11/09/2014.
		Esta spell abaixo � somente para remover o bug dessa core 11/09/2014. As cores anteriores n�o tem este bug.
		*/		// Remover Bug: [Seal of Righteousness]
		player->RemoveSpell(20154, true); // este comando dever ser retirada se aplica este script em outras cores.
		break;
	case CLASS_PRIEST:
		if (player->HasSpell(34914)) //Vampiric Touch Rank 1
			player->LearnSpell(48160, false);

		if (player->HasSpell(47540)) //Penance Rank 1
			player->LearnSpell(53007, false);

		if (player->HasSpell(724)) //LightWell Rank 1
			player->LearnSpell(48087, false);

		if (player->HasSpell(19236)) //Desperate Prayer Rank 1
			player->LearnSpell(48173, false);

		if (player->HasSpell(34861)) //Circle Of healing Rank 1
			player->LearnSpell(48089, false);

		if (player->HasSpell(15407)) //Mind Flay Rank 1
			player->LearnSpell(48156, false);

		player->LearnSpell(1180, false); // Weapon Daggers
		break;
	case CLASS_ROGUE:
		for (int i = 0; i < 6; i++)
		{
			player->LearnSpell(SRestante[6][i], false);
		}
		if (player->HasSpell(16511)) //Hemorrhage Rank 1
			player->LearnSpell(48660, false);

		if (player->HasSpell(1329)) //Mutilate Rank 1
			player->LearnSpell(48666, false);
		break;
	case CLASS_SHAMAN:
		for (int i = 0; i < 10; i++)
		{
			player->LearnSpell(SRestante[7][i], false);
		}
		if (player->GetTeam() == ALLIANCE)
		{
			player->LearnSpell(32182, false);
		}
		else{ // Horde
			player->LearnSpell(2825, false);
		}
		if (player->HasSpell(61295)) //Riptide Rank 1
			player->LearnSpell(61301, false);

		if (player->HasSpell(974)) //Earth Shield Rank 1
			player->LearnSpell(49284, false);

		if (player->HasSpell(30706)) //Totem of Wrath Rank 1
			player->LearnSpell(57722, false);

		if (player->HasSpell(51490)) //TunderStorm Rank 1
			player->LearnSpell(59159, false);
		break;
	case CLASS_WARLOCK:
		for (int i = 0; i < 6; i++)
		{
			player->LearnSpell(SRestante[8][i], false);
		}
		if (player->HasSpell(17877)) //ShadowBurn Rank 1
			player->LearnSpell(47827, false);

		if (player->HasSpell(30283)) //ShadowFury Rank 1
			player->LearnSpell(47847, false);

		if (player->HasSpell(50796)) //Chaos Bolt Rank 1
			player->LearnSpell(59172, false);

		if (player->HasSpell(30108)) //Unstable Affliction Rank 1
			player->LearnSpell(47843, false);

		if (player->HasSpell(48181)) //Spell Haunt Rank 1
			player->LearnSpell(59164, false);

		if (player->HasSpell(18220)) //Dark Pact Rank 1
			player->LearnSpell(59092, false);
		break;
	default:
		break;
	}
	return true;
}

bool TreinarSpell_autoEquipe(Player* player, uint8 plevelup)
{	
	if (plevelup == player->getLevel() + 1)
		return true;
	ChrClassesEntry const* classEntry = sChrClassesStore.LookupEntry(player->GetSession()->GetPlayer()->getClass());
	if (!classEntry)
		return true;
	uint32 family = classEntry->spellfamily;

	for (uint32 i = 0; i < sSkillLineAbilityStore.GetNumRows(); ++i)
	{
		SkillLineAbilityEntry const* entry = sSkillLineAbilityStore.LookupEntry(i);
		if (!entry)
			continue;

		SpellInfo const* spellInfo = sSpellMgr->GetSpellInfo(entry->spellId);
		if (!spellInfo)	continue;

		// skip server-side/triggered spells
		if (spellInfo->SpellLevel == 0)	continue;

		// skip wrong class/race skills
		if (!player->GetSession()->GetPlayer()->IsSpellFitByClassAndRace(spellInfo->Id)) continue;

		// skip other spell families
		if (spellInfo->SpellFamilyName != family) continue;

		// Pulas spells n�o validas ou spell de talentos que n�o poder ser treinado de imediato 
		// OBS: Lembre de quando distripuir seus talentos volte no npc para treinar as spell de talentos... 
		if (GetSpellIguinorar(spellInfo->Id))	continue;

		// skip spells with first rank learned as talent (and all talents then also)
		if (GetTalentSpellCost(spellInfo->GetFirstRankSpell()->Id) > 0)	continue;

		// skip broken spells
		if (!SpellMgr::IsSpellValid(spellInfo, player->GetSession()->GetPlayer(), false)) continue;

		// Spell Restante e Spell de Talentos
		if (!SpellsRestante(player, plevelup)) continue;

		// N�o inclui Teleports, mounts and other spells.
		if ((spellInfo->AttributesEx7 & SPELL_ATTR7_HORDE_ONLY && player->GetTeam() != HORDE)
			|| (spellInfo->AttributesEx7 & SPELL_ATTR7_ALLIANCE_ONLY && player->GetTeam() != ALLIANCE)) continue;

		// Add spell de acordo com o level. Skip spell level inv�lido
		if (spellInfo->BaseLevel != plevelup && sSpellMgr->IsSpellValid(spellInfo, player)) continue;

		bool VAbilidades = false;
		SkillLineAbilityMapBounds Limitar = sSpellMgr->GetSkillLineAbilityMapBounds(spellInfo->Id);
		for (SkillLineAbilityMap::const_iterator itr = Limitar.first; itr != Limitar.second; ++itr)
		{
			if (itr->second->spellId == spellInfo->Id && itr->second->racemask == 0 && itr->second->AutolearnType == 0)
			{
				VAbilidades = true;
				SpellInfo const* prevSpell = spellInfo->GetPrevRankSpell();
				if (prevSpell && !player->HasSpell(prevSpell->Id))
				{
					VAbilidades = false;
					break;
				}
				if (GetTalentSpellPos(itr->second->spellId))
				if (!prevSpell || !player->HasSpell(prevSpell->Id) || spellInfo->GetRank() == 1)
					VAbilidades = false;
				break;
			}
		}
		if (VAbilidades){
			if (player->HasSpell(spellInfo->Id)) continue;
			player->GetSession()->GetPlayer()->LearnSpell(spellInfo->Id, false);
		}
	}
	TreinarSpell_autoEquipe(player, ++plevelup); // Add spell de acordo com o level	
	//player->UpdateAllStats(); 
	player->UpdateSkillsToMaxSkillsForLevel();
	player->CastSpell(player, 47445, false); // Spell s� para visual		
	return true;
}

uint32 Tipo_de_Talentos = 0; // spec 1,2,3
struct LearnTalentosData
{	
	uint32 spellTalentos;
	uint32 pClass;
	uint32 pTalentoTipo;
};
std::vector<LearnTalentosData> LearnTalentosLista;
bool CarregarAutoEquipeSpellTalentos()
{
	LearnTalentosLista.clear();

	TC_LOG_INFO("server.loading", "Carregando spellTalentos para AutoEquipe...");
	uint32 sMSTime = getMSTime();

	QueryResult resultados = CharacterDatabase.PQuery("SELECT pTalentoTipo, spellTalentos, pClass FROM `character_talent_autoequipe`");
	if (!resultados)
		return true;
	uint16 contador = 0;
	do
	{
		Field* fields = resultados->Fetch();
		LearnTalentosData SpellTalents;
		SpellTalents.pTalentoTipo = fields[0].GetUInt32();
		SpellTalents.spellTalentos = fields[1].GetUInt32();
		SpellTalents.pClass = fields[2].GetUInt32();

		LearnTalentosLista.push_back(SpellTalents);
		++contador;
	} while (resultados->NextRow());

	TC_LOG_INFO("server.loading", ">> Carregadas %u spellTalentos para AutoEquipe em %u ms", contador, GetMSTimeDiffToNow(sMSTime));

	return true;
}

bool LearnTalentos(Player* player) //, uint32 acao)
{
	uint32 pClass = player->getClassMask();
	
	player->ResetTalents();
	//player->SetFreeTalentPoints(player->CalculateTalentsPoints());

	for (uint32 i = 0; i < LearnTalentosLista.size(); ++i)
	{
		LearnTalentosData &SpellTalents = LearnTalentosLista[i];
						
		if (!(SpellTalents.pClass & pClass)) continue;

		if (!(SpellTalents.pTalentoTipo == Tipo_de_Talentos)) continue;

		player->RemoveSpell(SpellTalents.spellTalentos, true, false);

		//if (player->HasTalent(SpellTalents.spellTalentos, 0)) continue;

		player->AddTalent(SpellTalents.spellTalentos,0,true);
		player->LearnSpell(SpellTalents.spellTalentos,false); 
	}
	player->SetFreeTalentPoints(0);

	Tipo_de_Talentos = NULL;
	return true;

}