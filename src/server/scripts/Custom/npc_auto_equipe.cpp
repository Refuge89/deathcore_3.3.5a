/*
 * Copyright (C) 2013-2015 DeathCore <http://www.noffearrdeathproject.net/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "ScriptPCH.h"
#include "npc_auto_equipe.h"

class npc_auto_equipe : public CreatureScript
{
public:
	npc_auto_equipe() : CreatureScript("npc_auto_equipe") {}

	bool OnGossipHello(Player* player, Creature* creature)
	{
		player->PlayerTalkClass->ClearMenus();
		if (player->IsInCombat())
		{
			player->CLOSE_GOSSIP_MENU();
			ChatHandler(player->GetSession()).SendSysMessage("|cffFF0000Voce esta em combate!|r");
			return true;
		}
		else{
			player->ADD_GOSSIP_ITEM(0, "Bem Vindo ao OrbiTWoW - Sistema de Equipamento", GOSSIP_SENDER_MAIN, MENU_VOLTAR);

			//player->ADD_GOSSIP_ITEM(5, "|cff008B8BEquipar|r", GOSSIP_SENDER_MAIN, MENU_EQUIPAR_ITEM_AUTOMATICOS);
			player->ADD_GOSSIP_ITEM(5, "|cff1C1C1CAplicar talentos e\n equipar-se|r", GOSSIP_SENDER_MAIN, AUTO_TALENTOS_MENU);
			//player->ADD_GOSSIP_ITEM(3, "|cff191970Trenador de Classes", GOSSIP_SENDER_MAIN, MENUTRENADOR);
			player->ADD_GOSSIP_ITEM(2, "|cffFF0000Sair", GOSSIP_SENDER_MAIN, MENU_SAIR);

			player->SEND_GOSSIP_MENU(1, creature->GetGUID());
			player->PlayerTalkClass->SendGossipMenu(99999, creature->GetGUID());
			return true;
		}
	}
	void MenuAutoTalent(Player* player, Creature* creature /*uint32 acao*/) // menu de talentos
	{
		player->PlayerTalkClass->ClearMenus();
		switch (player->getClass())
		{
		case CLASS_WARRIOR:
			player->ADD_GOSSIP_ITEM_EXTENDED(5, "|cffFF0000|TInterface\\icons\\Ability_Rogue_Eviscerate:40|t Arms",
				GOSSIP_SENDER_MAIN, SKILL_ARMS, "Tem certeza que quer se equipar e treinar seus Talentos para Arms?\n\n |cffFFFFFFPrecisa relogar-se para atualizar seus status.!|r", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(5, "|cffFF0000|TInterface\\icons\\Ability_Warrior_InnerRage:40|t Fury",
				GOSSIP_SENDER_MAIN, SKILL_FURY, "Tem certeza que quer se equipar e treinar seus Talentos para Fury?\n |cffFFFFFFPrecisa relogar-se para atualizar seus status.!|r", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(5, "|cffFF0000|TInterface\\icons\\Ability_Warrior_DefensiveStance:40|t Protection",
				GOSSIP_SENDER_MAIN, SKILL_PROTECTION, "Tem certeza que quer se equipar e treinar seus Talentos para Protection?\n |cffFFFFFFPrecisa relogar-se para atualizar seus status.!|r", 0, false);

			break;
		case CLASS_PRIEST: // Prieste � SKILL_HOLY e Paladin SKILL_HOLY2
			player->ADD_GOSSIP_ITEM_EXTENDED(5, "|cffFF0000|TInterface\\icons\\Spell_Holy_WordFortitude:40|t Dicipline",
				GOSSIP_SENDER_MAIN, SKILL_DISCIPLINE, "Tem certeza que quer se equipar e treinar seus Talentos para Dicipline?\n |cffFFFFFFPrecisa relogar-se para atualizar seus status.!|r", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(5, "|cffFF0000|TInterface\\icons\\Spell_Holy_GuardianSpirit:40|t Hole",
				GOSSIP_SENDER_MAIN, SKILL_HOLY, "Tem certeza que quer se equipar e treinar seus Talentos para Hole?\n |cffFFFFFFPrecisa relogar-se para atualizar seus status.!|r", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(5, "|cffFF0000|TInterface\\icons\\Spell_Shadow_DemonicEmpathy:40|t Shadow",
				GOSSIP_SENDER_MAIN, SKILL_SHADOW, "Tem certeza que quer se equipar e treinar seus Talentos para Shadow?\n |cffFFFFFFPrecisa relogar-se para atualizar seus status.!|r", 0, false);

			break;
		case CLASS_DRUID:
			player->ADD_GOSSIP_ITEM_EXTENDED(5, "|cffFF0000|TInterface\\icons\\Spell_Nature_StarFall:40|t Balance",
				GOSSIP_SENDER_MAIN, SKILL_BALANCE, "Tem certeza que quer se equipar e treinar seus Talentos para Balance?\n |cffFFFFFFPrecisa relogar-se para atualizar seus status.!|r", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(5, "|cffFF0000|TInterface\\icons\\Spell_Nature_GiftoftheWaterSpirit:40|t Feral",
				GOSSIP_SENDER_MAIN, SKILL_FERAL_COMBAT, "Tem certeza que quer se equipar e treinar seus Talentos para Feral?\n |cffFFFFFFPrecisa relogar-se para atualizar seus status.!|r", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(5, "|cffFF0000|TInterface\\icons\\spell_nature_healingtouch:40|t Restoration",
				GOSSIP_SENDER_MAIN, SKILL_RESTORATION2, "Tem certeza que quer se equipar e treinar seus Talentos para Restoration?\n |cffFFFFFFPrecisa relogar-se para atualizar seus status.!|r", 0, false);

			break;
		case CLASS_DEATH_KNIGHT:
			player->ADD_GOSSIP_ITEM_EXTENDED(5, "|cffFF0000|TInterface\\icons\\Spell_Deathknight_BloodPresence:40|t Blood",
				GOSSIP_SENDER_MAIN, SKILL_DK_BLOOD, "Tem certeza que quer se equipar e treinar seus Talentos para Blood?\n |cffFFFFFFPrecisa relogar-se para atualizar seus status.!|r", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(5, "|cffFF0000|TInterface\\icons\\Spell_Deathknight_FrostPresence:40|t Frost",
				GOSSIP_SENDER_MAIN, SKILL_DK_FROST, "Tem certeza que quer se equipar e treinar seus Talentos para Forst?\n |cffFFFFFF0Precisa relogar-se para atualizar seus status.!|r", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(5, "|cffFF0000|TInterface\\icons\\Spell_Deathknight_UnholyPresence:40|t Unholy",
				GOSSIP_SENDER_MAIN, SKILL_DK_UNHOLY, "Tem certeza que quer se equipar e treinar seus Talentos para Unholy?\n |cffFFFFFFPrecisa relogar-se para atualizar seus status.!|r", 0, false);

			break;
		case CLASS_MAGE:
			player->ADD_GOSSIP_ITEM_EXTENDED(5, "|cffFF0000|TInterface\\icons\\Spell_Holy_ArcaneIntellect:40|t Arcane",
				GOSSIP_SENDER_MAIN, SKILL_ARCANE, "Tem certeza que quer se equipar e treinar seus Talentos para Arcane?\n |cffFFFFFFPrecisa relogar-se para atualizar seus status.!|r", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(5, "|cffFF0000|TInterface\\icons\\Spell_Fire_FireBolt02:40|t Fire",
				GOSSIP_SENDER_MAIN, SKILL_FIRE, "Tem certeza que quer se equipar e treinar seus Talentos para Fire?\n |cffFFFFFFPrecisa relogar-se para atualizar seus status.!|r", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(5, "|cffFF0000|TInterface\\icons\\Spell_Frost_FrostBolt02:40|t Frost",
				GOSSIP_SENDER_MAIN, SKILL_FROST, "Tem certeza que quer se equipar e treinar seus Talentos para Frost?\n |cffFFFFFFPrecisa relogar-se para atualizar seus status.!|r", 0, false);

			break;
		case CLASS_HUNTER:
			player->ADD_GOSSIP_ITEM_EXTENDED(5, "|cffFF0000|TInterface\\icons\\ability_hunter_beasttaming:40|t Beast Master",
				GOSSIP_SENDER_MAIN, SKILL_BEAST_MASTERY, "Tem certeza que quer se equipar e treinar seus Talentos para Beast Master?\n |cffFFFFFFPrecisa relogar-se para atualizar seus status.!|r", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(5, "|cffFF0000|TInterface\\icons\\ability_hunter_rapidregeneration:40|t Marksmanship",
				GOSSIP_SENDER_MAIN, SKILL_MARKSMANSHIP, "Tem certeza que quer se equipar e treinar seus Talentos para Marksmanship?\n |cffFFFFFFPrecisa relogar-se para atualizar seus status.!|r", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(5, "|cffFF0000|TInterface\\icons\\Spell_DeathKnight_Butcher:40|t Survival",
				GOSSIP_SENDER_MAIN, SKILL_SURVIVAL, "Tem certeza que quer se equipar e treinar seus Talentos para Survival?\n |cffFFFFFFPrecisa relogar-se para atualizar seus status.!|r", 0, false);

			break;
		case CLASS_PALADIN:
			player->ADD_GOSSIP_ITEM_EXTENDED(5, "|cffFF0000|TInterface\\icons\\spell_holy_holybolt:40|t Holy",
				GOSSIP_SENDER_MAIN, SKILL_HOLY2, "Tem certeza que quer se equipar e treinar seus Talentos para Holy?\n |cffFFFFFFPrecisa relogar-se para atualizar seus status.!|r", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(5, "|cffFF0000|TInterface\\icons\\SPELL_HOLY_DEVOTIONAURA:40|t Protection",
				GOSSIP_SENDER_MAIN, SKILL_PROTECTION2, "Tem certeza que quer se equipar e treinar seus Talentos para Protection?\n |cffFFFFFFPrecisa relogar-se para atualizar seus status.!|r", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(5, "|cffFF0000|TInterface\\icons\\Spell_Holy_AuraOfLight:40|t Retribution",
				GOSSIP_SENDER_MAIN, SKILL_RETRIBUTION, "Tem certeza que quer se equipar e treinar seus Talentos para Retribution?\n |cffFFFFFFPrecisa relogar-se para atualizar seus status.!|r", 0, false);

			break;
		case CLASS_ROGUE:
			player->ADD_GOSSIP_ITEM_EXTENDED(5, "|cffFF0000|TInterface\\icons\\Ability_Rogue_Eviscerate:40|t Assassination",
				GOSSIP_SENDER_MAIN, SKILL_ASSASSINATION, "Tem certeza que quer se equipar e treinar seus Talentos para Assassination?\n |cffFFFFFFPrecisa relogar-se para atualizar seus status.!|r", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(5, "|cffFF0000|TInterface\\icons\\ability_backstab:40|t Combat",
				GOSSIP_SENDER_MAIN, SKILL_COMBAT, "Tem certeza que quer se equipar e treinar seus Talentos para Combat?\n |cffFFFFFFPrecisa relogar-se para atualizar seus status.!|r", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(5, "|cffFF0000|TInterface\\icons\\Ability_Stealth:40|t Subtlety",
				GOSSIP_SENDER_MAIN, SKILL_SUBTLETY, "Tem certeza que quer se equipar e treinar seus Talentos para Subtlety?\n |cffFFFFFFPrecisa relogar-se para atualizar seus status.!|r", 0, false);

			break;
		case CLASS_SHAMAN:
			player->ADD_GOSSIP_ITEM_EXTENDED(5, "|cffFF0000|TInterface\\icons\\spell_nature_stormreach:40|t Elemental",
				GOSSIP_SENDER_MAIN, SKILL_ELEMENTAL_COMBAT, "Tem certeza que quer se equipar e treinar seus Talentos para Elemental?\n |cffFFFFFFPrecisa relogar-se para atualizar seus status.!|r", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(5, "|cffFF0000|TInterface\\icons\\Spell_Nature_LightningShield:40|t Enhancement",
				GOSSIP_SENDER_MAIN, SKILL_ENHANCEMENT, "Tem certeza que quer se equipar e treinar seus Talentos para Enhancement?\n |cffFFFFFFPrecisa relogar-se para atualizar seus status.!|r", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(5, "|cffFF0000|TInterface\\icons\\spell_nature_healingwavelesser:40|t Restoration",
				GOSSIP_SENDER_MAIN, SKILL_RESTORATION, "Tem certeza que quer se equipar e treinar seus Talentos para Restoration?\n |cffFFFFFFPrecisa relogar-se para atualizar seus status.!|r", 0, false);

			break;
		case CLASS_WARLOCK:
			player->ADD_GOSSIP_ITEM_EXTENDED(5, "|cffFF0000|TInterface\\icons\\Spell_Shadow_DeathCoil:40|t Affliction",
				GOSSIP_SENDER_MAIN, SKILL_AFFLICTION, "Tem certeza que quer se equipar e treinar seus Talentos para Affliction?\n |cffFFFFFFPrecisa relogar-se para atualizar seus status.!|r", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(5, "|cffFF0000|TInterface\\icons\\Spell_Shadow_Metamorphosis:40|t Demonology",
				GOSSIP_SENDER_MAIN, SKILL_DEMONOLOGY, "Tem certeza que quer se equipar e treinar seus Talentos para Demonology?\n |cffFFFFFFPrecisa relogar-se para atualizar seus status.!|r", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(5, "|cffFF0000|TInterface\\icons\\spell_shadow_rainoffire:40|t Destrution",
				GOSSIP_SENDER_MAIN, SKILL_DESTRUCTION, "Tem certeza que quer se equipar e treinar seus Talentos para Destrution?\n |cffFFFFFFPrecisa relogar-se para atualizar seus status.!|r", 0, false);

			break;
		}
		player->ADD_GOSSIP_ITEM(2, "|cff0000FF <<Voltar|r", GOSSIP_SENDER_MAIN, MENU_VOLTAR);
		player->ADD_GOSSIP_ITEM(2, "|cffFF0000Sair", GOSSIP_SENDER_MAIN, MENU_SAIR);
		player->SEND_GOSSIP_MENU(1, creature->GetGUID());
		player->PlayerTalkClass->SendGossipMenu(9999, creature->GetGUID());
		return;
	}
	void MenuAutoEquipe(Player* player, Creature* creature, uint32 menuClass, uint32 acao)
	{
		player->PlayerTalkClass->ClearMenus();
		switch (player->getClass())
		{
		case CLASS_WARRIOR:
			player->ADD_GOSSIP_ITEM_EXTENDED(5, "|cffFF0000|TInterface\\icons\\inv_chest_plate_23:40|t"+Arena_Season_Name,
				GOSSIP_SENDER_MAIN, EQUIPAR_ITEM_AUTOMATICOS, "Tem certza que quer equipar automaticamente \n\n|cffC71585"+Arena_Season_Name+"|r", 0, false);
			break;
		case CLASS_PRIEST: // Prieste � SKILL_HOLY e Paladin SKILL_HOLY2
			player->ADD_GOSSIP_ITEM_EXTENDED(5, "|cffFF0000|TInterface\\icons\\inv_chest_cloth_75:40|t" + Arena_Season_Name,
				GOSSIP_SENDER_MAIN, EQUIPAR_ITEM_AUTOMATICOS, "Tem certza que quer equipar automaticamente \n\n|cffC71585" + Arena_Season_Name + "|r", 0, false);
			break;
		case CLASS_DRUID:
			player->ADD_GOSSIP_ITEM_EXTENDED(5, "|cffFF0000|TInterface\\icons\\inv_chest_leather_16:40|t" + Arena_Season_Name,
				GOSSIP_SENDER_MAIN, EQUIPAR_ITEM_AUTOMATICOS, "Tem certza que quer equipar automaticamente \n\n|cffC71585" + Arena_Season_Name + "|r", 0, false);
			break;
		case CLASS_DEATH_KNIGHT:
			player->ADD_GOSSIP_ITEM_EXTENDED(5, "|cffFF0000|TInterface\\icons\\inv_chest_plate_23:40|t" + Arena_Season_Name,
				GOSSIP_SENDER_MAIN, EQUIPAR_ITEM_AUTOMATICOS, "Tem certza que quer equipar automaticamente \n\n|cffC71585" + Arena_Season_Name + "|r", 0, false);
			break;
		case CLASS_MAGE:
			player->ADD_GOSSIP_ITEM_EXTENDED(5, "|cffFF0000|TInterface\\icons\\inv_chest_cloth_75:40|t" + Arena_Season_Name,
				GOSSIP_SENDER_MAIN, EQUIPAR_ITEM_AUTOMATICOS, "Tem certza que quer equipar automaticamente \n\n|cffC71585" + Arena_Season_Name + "|r", 0, false);
			break;
		case CLASS_HUNTER:
			player->ADD_GOSSIP_ITEM_EXTENDED(5, "|cffFF0000|TInterface\\icons\\inv_chest_mail_03:40|t" + Arena_Season_Name,
				GOSSIP_SENDER_MAIN, EQUIPAR_ITEM_AUTOMATICOS, "Tem certza que quer equipar automaticamente \n\n|cffC71585" + Arena_Season_Name + "|r", 0, false);
			break;
		case CLASS_PALADIN:
			player->ADD_GOSSIP_ITEM_EXTENDED(5, "|cffFF0000|TInterface\\icons\\inv_chest_plate_23:40|t" + Arena_Season_Name,
				GOSSIP_SENDER_MAIN, EQUIPAR_ITEM_AUTOMATICOS, "Tem certza que quer equipar automaticamente \n\n|cffC71585" + Arena_Season_Name + "|r", 0, false);
			break;
		case CLASS_ROGUE:
			player->ADD_GOSSIP_ITEM_EXTENDED(5, "|cffFF0000|TInterface\\icons\\inv_chest_leather_16:40|t" + Arena_Season_Name,
				GOSSIP_SENDER_MAIN, EQUIPAR_ITEM_AUTOMATICOS, "Tem certza que quer equipar automaticamente \n\n|cffC71585" + Arena_Season_Name + "|r", 0, false);
			break;
		case CLASS_SHAMAN:
			player->ADD_GOSSIP_ITEM_EXTENDED(5, "|cffFF0000|TInterface\\icons\\inv_chest_mail_03:40|t" + Arena_Season_Name,
				GOSSIP_SENDER_MAIN, EQUIPAR_ITEM_AUTOMATICOS, "Tem certza que quer equipar automaticamente \n\n|cffC71585" + Arena_Season_Name + "|r", 0, false);
			break;
		case CLASS_WARLOCK:
			player->ADD_GOSSIP_ITEM_EXTENDED(5, "|cffFF0000|TInterface\\icons\\inv_chest_cloth_75:40|t" + Arena_Season_Name,
				GOSSIP_SENDER_MAIN, EQUIPAR_ITEM_AUTOMATICOS, "Tem certza que quer equipar automaticamente \n\n|cffC71585" + Arena_Season_Name + "|r", 0, false);
			break;
		}
		player->ADD_GOSSIP_ITEM(2, "|cff0000FF <<Voltar|r", GOSSIP_SENDER_MAIN, MENU_VOLTAR);
		player->ADD_GOSSIP_ITEM(2, "|cffFF0000Sair", GOSSIP_SENDER_MAIN, MENU_SAIR);
		player->SEND_GOSSIP_MENU(1, creature->GetGUID());
		player->PlayerTalkClass->SendGossipMenu(9999, creature->GetGUID());
		return;
	}
	void ShoWMenuTrenador(Player* player, Creature* creature, uint32 menuClass, uint32 acao) //Menu trainer
	{
		player->PlayerTalkClass->ClearMenus();
		switch (player->getClass())
		{
		case CLASS_WARRIOR:
			player->ADD_GOSSIP_ITEM(3, "|cff191970|TInterface\\icons\\Spell_Shadow_TwistedFaith:30|t Trenador de classes|r   |cff000000|TInterface\\icons\\ability_warrior_warcry:30|t", GOSSIP_SENDER_MAIN, TRENADORCLASS);
			break;
		case CLASS_DEATH_KNIGHT:
			player->ADD_GOSSIP_ITEM(3, "|cff191970|TInterface\\icons\\Spell_Shadow_TwistedFaith:30|t Trenador de classes|r   |cff000000|TInterface\\icons\\inv_sword_62:30|t", GOSSIP_SENDER_MAIN, TRENADORCLASS);
			break;
		case CLASS_DRUID:
			player->ADD_GOSSIP_ITEM(3, "|cff191970|TInterface\\icons\\Spell_Shadow_TwistedFaith:30|t Trenador de classes|r   |cff000000|TInterface\\icons\\ability_druid_flightform:30|t", GOSSIP_SENDER_MAIN, TRENADORCLASS);
			break;
		case CLASS_HUNTER:
			player->ADD_GOSSIP_ITEM(3, "|cff191970|TInterface\\icons\\Spell_Shadow_TwistedFaith:30|t Trenador de classes|r   |cff000000|TInterface\\icons\\ability_hunter_beasttaming:30|t", GOSSIP_SENDER_MAIN, TRENADORCLASS);
			break;
		case CLASS_MAGE:
			player->ADD_GOSSIP_ITEM(3, "|cff191970|TInterface\\icons\\Spell_Shadow_TwistedFaith:30|t Trenador de classes|r   |cff000000|TInterface\\icons\\spell_arcane_portalironforge:30|t", GOSSIP_SENDER_MAIN, TRENADORCLASS);
			break;
		case CLASS_PALADIN:
			player->ADD_GOSSIP_ITEM(3, "|cff191970|TInterface\\icons\\Spell_Shadow_TwistedFaith:30|t Trenador de classes|r   |cff000000|TInterface\\icons\\spell_holy_crusaderstrike:30|t", GOSSIP_SENDER_MAIN, TRENADORCLASS);
			break;
		case CLASS_PRIEST:
			player->ADD_GOSSIP_ITEM(3, "|cff191970|TInterface\\icons\\Spell_Shadow_TwistedFaith:30|t Trenador de classes|r   |cff000000|TInterface\\icons\\spell_holy_powerwordshield:30|t", GOSSIP_SENDER_MAIN, TRENADORCLASS);
			break;
		case CLASS_ROGUE:
			player->ADD_GOSSIP_ITEM(3, "|cff191970|TInterface\\icons\\Spell_Shadow_TwistedFaith:30|t Trenador de classes|r   |cff000000|TInterface\\icons\\ability_stealth:30|t", GOSSIP_SENDER_MAIN, TRENADORCLASS);
			break;
		case CLASS_SHAMAN:
			player->ADD_GOSSIP_ITEM(3, "|cff191970|TInterface\\icons\\Spell_Shadow_TwistedFaith:30|t Trenador de classes|r   |cff000000|TInterface\\icons\\spell_nature_bloodlust:30|t", GOSSIP_SENDER_MAIN, TRENADORCLASS);
			break;
		case CLASS_WARLOCK:
			player->ADD_GOSSIP_ITEM(3, "|cff191970|TInterface\\icons\\Spell_Shadow_TwistedFaith:30|t Trenador de classes|r   |cff000000|TInterface\\icons\\spell_shadow_possession:30|t", GOSSIP_SENDER_MAIN, TRENADORCLASS);
			break;
		}
		if (TIPO_DE_TRENADOR_ORIGINAL == 1)
			player->ADD_GOSSIP_ITEM(3, "|cffFF0000|TInterface\\icons\\Mail_GMIcon:30|t Trenadores Originais|r", GOSSIP_SENDER_MAIN, MENU_TRENADOR_SPELLSUMMON); //n�o requer script multiTrainer

		if (TIPO_DE_TRENADOR_ORIGINAL == 2)
			player->ADD_GOSSIP_ITEM(3, "|cffFF0000|TInterface\\icons\\Ability_Vehicle_LaunchPlayer:30|t Trenadores Originais|r", GOSSIP_SENDER_MAIN, MENU_TRENADOR_ORIGINAL); //requer script muitTrainer
			player->ADD_GOSSIP_ITEM_EXTENDED(3, "|cffCD2626|TInterface\\icons\\Ability_DualWield:30|t Especializacao Talentos Duplos|r", GOSSIP_SENDER_MAIN, DUAL_TELENTS, "Voce tem certesa que quer compra\nEspecializacao Telentos Duplos\n", 3 * GOLD, false);
			player->ADD_GOSSIP_ITEM(2, "|cffFF0000|TInterface\\icons\\INV_Inscription_MinorGlyph20:30|t Comprar Glyphs|r", GOSSIP_SENDER_MAIN, GLYPH_NPC_MENU);
		
			//		player->ADD_GOSSIP_ITEM_EXTENDED(10, "|cffFF0000|TInterface\\icons\\INV_Jewelry_FrostwolfTrinket_01:30|t Reste total Level 80", GOSSIP_SENDER_MAIN, RESTAR_LEVEL80, "ALERTA!\nSera restado ate ao level 1", 0, false);
			player->ADD_GOSSIP_ITEM(32, "|cffFF0000|TInterface\\icons\\Achievement_BG_hld4bases_EOS:40|t Resetar Talentos", GOSSIP_SENDER_MAIN, RESET_TALENTOS);
			player->ADD_GOSSIP_ITEM(2, "|cff0000FF <<Voltar|r", GOSSIP_SENDER_MAIN, MENU_VOLTAR);
			player->ADD_GOSSIP_ITEM(2, "|cffFF0000Sair", GOSSIP_SENDER_MAIN, MENU_SAIR);
			player->SEND_GOSSIP_MENU(1, creature->GetGUID());
			player->PlayerTalkClass->SendGossipMenu(9999, creature->GetGUID());
			return;
	}	
	bool OnGossipSelect(Player *player, Creature *creature, uint32  sender, uint32 action)
	{
		player->PlayerTalkClass->ClearMenus();
		switch (action)
		{
			//warrio arms
		case SKILL_ARMS:
			Tipo_de_Talentos = 26;
			player->CLOSE_GOSSIP_MENU();
			creature->CastSpell(player, 26410, false); // Spell s� para visual
			ExecutarALLTalentos(player, sender);			
			break;
			//warrio fury
		case SKILL_FURY:
			Tipo_de_Talentos = 256;
			player->CLOSE_GOSSIP_MENU();
			creature->CastSpell(player, 26410, false); // Spell s� para visual
			ExecutarALLTalentos(player, sender);
			break;
			//warrios protection
		case SKILL_PROTECTION: // Warrior � Protection
			Tipo_de_Talentos = 257;
			player->CLOSE_GOSSIP_MENU();
			creature->CastSpell(player, 26410, false); // Spell s� para visual
			ExecutarALLTalentos(player, sender);
			break;
			//paladin
		case SKILL_HOLY2: // Paladin � SKILL_HOLY2
			Tipo_de_Talentos = 594;
			player->CLOSE_GOSSIP_MENU();
			creature->CastSpell(player, 26410, false); // Spell s� para visual
			ExecutarALLTalentos(player, sender);
			break;
			//paladin
		case SKILL_RETRIBUTION:
			Tipo_de_Talentos = 184;
			player->CLOSE_GOSSIP_MENU();
			creature->CastSpell(player, 26410, false); // Spell s� para visual
			ExecutarALLTalentos(player, sender);
			break;
			//paladin
		case SKILL_PROTECTION2: // Paladin � Protection2
			Tipo_de_Talentos = 267;
			player->CLOSE_GOSSIP_MENU();
			creature->CastSpell(player, 26410, false); // Spell s� para visual
			ExecutarALLTalentos(player, sender);
			break;
			//priest dicipline
		case SKILL_DISCIPLINE:
			Tipo_de_Talentos = 613;
			player->CLOSE_GOSSIP_MENU();
			creature->CastSpell(player, 26410, false); // Spell s� para visual
			ExecutarALLTalentos(player, sender);
			break;
			//preiste hole
		case SKILL_HOLY: // Prieste � SKILL_HOLY 
			Tipo_de_Talentos = 56;
			player->CLOSE_GOSSIP_MENU();
			creature->CastSpell(player, 26410, false); // Spell s� para visual
			ExecutarALLTalentos(player, sender);
			break;
			//prieste shadow
		case SKILL_SHADOW:
			Tipo_de_Talentos = 78;
			player->CLOSE_GOSSIP_MENU();
			creature->CastSpell(player, 26410, false); // Spell s� para visual
			ExecutarALLTalentos(player, sender);
			break;
			// druid balance
		case SKILL_BALANCE:
			Tipo_de_Talentos = 574;
			player->CLOSE_GOSSIP_MENU();
			creature->CastSpell(player, 26410, false); // Spell s� para visual
			ExecutarALLTalentos(player, sender);
			break;
			// druid feral
		case SKILL_FERAL_COMBAT:
			Tipo_de_Talentos = 134;
			player->CLOSE_GOSSIP_MENU();
			creature->CastSpell(player, 26410, false); // Spell s� para visual
			ExecutarALLTalentos(player, sender);
			break;
			// druid restoration
		case SKILL_RESTORATION2: // Druid Restoration2
			Tipo_de_Talentos = 573;
			player->CLOSE_GOSSIP_MENU();
			creature->CastSpell(player, 26410, false); // Spell s� para visual
			ExecutarALLTalentos(player, sender);
			break;
			// dk blood
		case SKILL_DK_BLOOD:
			Tipo_de_Talentos = 770;
			player->CLOSE_GOSSIP_MENU();
			creature->CastSpell(player, 26410, false); // Spell s� para visual
			ExecutarALLTalentos(player, sender);
			break;
			// dk frost
		case SKILL_DK_FROST:
			Tipo_de_Talentos = 771;
			player->CLOSE_GOSSIP_MENU();
			creature->CastSpell(player, 26410, false); // Spell s� para visual
			ExecutarALLTalentos(player, sender);
			break;
			// dk unholy
		case SKILL_DK_UNHOLY:
			Tipo_de_Talentos = 772;
			player->CLOSE_GOSSIP_MENU();
			creature->CastSpell(player, 26410, false); // Spell s� para visual
			ExecutarALLTalentos(player, sender);
			break;
			// mage acane
		case SKILL_ARCANE:
			Tipo_de_Talentos = 237;
			player->CLOSE_GOSSIP_MENU();
			creature->CastSpell(player, 26410, false); // Spell s� para visual
			ExecutarALLTalentos(player, sender);
			break;
			// mage fire
		case SKILL_FIRE:
			Tipo_de_Talentos = 8;
			player->CLOSE_GOSSIP_MENU();
			creature->CastSpell(player, 26410, false); // Spell s� para visual
			ExecutarALLTalentos(player, sender);
			break;
			// mage frost
		case SKILL_FROST:
			Tipo_de_Talentos = 6;
			player->CLOSE_GOSSIP_MENU();
			creature->CastSpell(player, 26410, false); // Spell s� para visual
			ExecutarALLTalentos(player, sender);
			break;
			// hunter master
		case SKILL_BEAST_MASTERY:
			Tipo_de_Talentos = 50;
			player->CLOSE_GOSSIP_MENU();
			creature->CastSpell(player, 26410, false); // Spell s� para visual
			ExecutarALLTalentos(player, sender);
			break;
			// hunter markshanship
		case SKILL_MARKSMANSHIP:
			Tipo_de_Talentos = 163;
			player->CLOSE_GOSSIP_MENU();
			creature->CastSpell(player, 26410, false); // Spell s� para visual
			ExecutarALLTalentos(player, sender);
			break;
			// hunter suvival
		case SKILL_SURVIVAL:
			Tipo_de_Talentos = 51;
			player->CLOSE_GOSSIP_MENU();
			creature->CastSpell(player, 26410, false); // Spell s� para visual
			ExecutarALLTalentos(player, sender);
			break;
			//shaman elemental
		case SKILL_ELEMENTAL_COMBAT:
			Tipo_de_Talentos = 375;
			player->CLOSE_GOSSIP_MENU();
			creature->CastSpell(player, 26410, false); // Spell s� para visual
			ExecutarALLTalentos(player, sender);
			break;
			// shaman / enchancement
		case SKILL_ENHANCEMENT:
			Tipo_de_Talentos = 373;
			player->CLOSE_GOSSIP_MENU();
			creature->CastSpell(player, 26410, false); // Spell s� para visual
			ExecutarALLTalentos(player, sender);
			break;
			// shaman restoration
		case SKILL_RESTORATION: // Shaman � restoration
			Tipo_de_Talentos = 374;
			player->CLOSE_GOSSIP_MENU();
			creature->CastSpell(player, 26410, false); // Spell s� para visual
			ExecutarALLTalentos(player, sender);
			break;
			//rogue
		case SKILL_ASSASSINATION:
			Tipo_de_Talentos = 253;
			player->CLOSE_GOSSIP_MENU();
			creature->CastSpell(player, 26410, false); // Spell s� para visual
			ExecutarALLTalentos(player, sender);
			break;
			//rogue
		case SKILL_COMBAT:
			Tipo_de_Talentos = 38;
			player->CLOSE_GOSSIP_MENU();
			creature->CastSpell(player, 26410, false); // Spell s� para visual
			ExecutarALLTalentos(player, sender);
			break;
			//rogue
		case SKILL_SUBTLETY:
			Tipo_de_Talentos = 39;
			player->CLOSE_GOSSIP_MENU();
			creature->CastSpell(player, 26410, false); // Spell s� para visual
			ExecutarALLTalentos(player, sender);
			break;
			//warlock
		case SKILL_AFFLICTION:
			Tipo_de_Talentos = 355;
			player->CLOSE_GOSSIP_MENU();
			creature->CastSpell(player, 26410, false); // Spell s� para visual
			ExecutarALLTalentos(player, sender);
			break;
			//warlock
		case SKILL_DEMONOLOGY:
			Tipo_de_Talentos = 354;
			player->CLOSE_GOSSIP_MENU();
			creature->CastSpell(player, 26410, false); // Spell s� para visual
			ExecutarALLTalentos(player, sender);
			break;
			//warlock
		case SKILL_DESTRUCTION:
			Tipo_de_Talentos = 593;
			player->CLOSE_GOSSIP_MENU();
			creature->CastSpell(player, 26410, false); // Spell s� para visual
			ExecutarALLTalentos(player, sender);
			break;

			//====================================
		case MENU_SAIR:
			player->CLOSE_GOSSIP_MENU();
			break;
		case MENU_VOLTAR:
			OnGossipHello(player, creature);
			break;
			//====================================
		case AUTO_TALENTOS_MENU:
			MenuAutoTalent(player, creature);
			break;

		case MENUTRENADOR:
			player->CLOSE_GOSSIP_MENU();
			ShoWMenuTrenador(player, creature, sender, action);
			break;
		case TRENADORCLASS:
			player->CLOSE_GOSSIP_MENU();
			creature->CastSpell(player, 26410, false); // Spell s� para visual
			TreinadorAutoEquipeAtalho(player, sender);
			break;

		case MENU_EQUIPAR_ITEM_AUTOMATICOS:
			player->CLOSE_GOSSIP_MENU();
			MenuAutoEquipe(player, creature, sender, action);
			break;
		case EQUIPAR_ITEM_AUTOMATICOS:
			player->CLOSE_GOSSIP_MENU();
			creature->CastSpell(player, 26410, false); // Spell s� para visual
			ExecutarALLItens(player, action);
			break;

		case GLYPH_NPC_MENU: //Requer MultiVendor
//			VendedorClyphs(player, creature);
			player->CLOSE_GOSSIP_MENU();
			break;
		case MENU_TRENADOR_ORIGINAL: //requer script muitTrainer
	//		TrenadoresOriginais(player, creature, action);
			player->CLOSE_GOSSIP_MENU();
			break;
		case MENU_TRENADOR_SPELLSUMMON: // N�o Requer multiTrainer
			SumonarTrainerPorSpell(player, creature, action);
			player->CLOSE_GOSSIP_MENU();
			break;

		case DUAL_TELENTS:
			EspecialzacaoDuelTalents(player);
			break;
		case RESET_TALENTOS:
			RestarTalentos(player, creature);
			break;
		}
		return true; // retornar switch (action)
	}	

};


class PlayerAutoEquipar : public PlayerScript
{
public:
	PlayerAutoEquipar() : PlayerScript("PlayerAutoEquipar") {}
	
	void OnLogin(Player* player, bool)
	{

	}

	
};

void AddSC_npc_auto_equipe()
{
	new npc_auto_equipe();
	new npc_auto_equipe_WorldScript();
	new AutoEquipe_commands();
	//new PlayerAutoEquipar();
}